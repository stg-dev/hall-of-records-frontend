<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class InitialDatabaseStructure extends Migration
{
    public string $filename = __DIR__ . '/../dumps/initial_database_structure.sql';

    public function up()
    {
        if (!file_exists($this->filename)) {
            echo "File {$this->filename} not found.\n";

            exit(1);
        }

        if (!DB::unprepared(file_get_contents($this->filename))) {
            echo "Importing of {$this->filename} failed.\n";

            exit(1);
        }
    }
}
