<?php

namespace Database\Seeders;

use App\Models\Game;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DemoDataSeeder extends Seeder
{
    public string $filename = __DIR__ . '/../dumps/demo_data.sql';

    public function run(): void
    {
        if (Game::query()->count() === 0) {
            echo "Game table is empty, seeding demo data.\n";
            if (!file_exists($this->filename)) {
                echo "File {$this->filename} not found. Unzipping archived demo data.\n";

                $zippable = false;
                $output = '';
                exec('which bunzip2', $output, $zippable);
                $zippable = $zippable === 0;
                if (!$zippable) {
                    echo 'bunzip2 does not seem to be installed on your system.'
                        . " Please install it to continue. Exiting.\n";
                    exit(1);
                }

                echo "Unzipping {$this->filename}.\n";
                system("bunzip2 -k {$this->filename}.bz2");
            }

            echo 'Importing demo data... ';
            if (!DB::unprepared(file_get_contents($this->filename))) {
                echo "Importing of {$this->filename} failed.\n";

                exit(1);
            }
            echo "Done.\n";
        }
    }
}
