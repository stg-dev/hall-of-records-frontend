<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Team;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    public function run()
    {
        if (!User::query()->where('email', 'admin@example.com')->exists()) {
            User::create([
                'email' => 'admin@example.com',
                'name' => 'Administrator',
                'password' => Hash::make('password'),
            ]);

            $team = new Team();
            $team->forceFill([
                'user_id' => 1,
                'name' => 'Administrators',
                'personal_team' => 1,
            ])->save();
        }
    }
}
