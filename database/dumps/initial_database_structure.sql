-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 15, 2022 at 10:55 PM
-- Server version: 10.7.3-MariaDB
-- PHP Version: 8.1.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


-- --------------------------------------------------------

--
-- Table structure for table `stg_companies`
--

CREATE TABLE `stg_companies` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `name_translit` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `name_filter` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_counterstops`
--

CREATE TABLE `stg_counterstops` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `type` varchar(16) COLLATE utf8mb3_unicode_ci NOT NULL,
  `score` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_games`
--

CREATE TABLE `stg_games` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `name_translit` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `name_filter` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `description` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `translations` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_game_categories`
--

CREATE TABLE `stg_game_categories` (
  `id` int(11) NOT NULL,
  `game_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_game_category_values`
--

CREATE TABLE `stg_game_category_values` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `value` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_game_links`
--

CREATE TABLE `stg_game_links` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `locale_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `title` varchar(100) COLLATE utf8mb3_unicode_ci NOT NULL,
  `url` varchar(500) COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_locales`
--

CREATE TABLE `stg_locales` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `value` varchar(16) COLLATE utf8mb3_unicode_ci NOT NULL,
  `title` varchar(32) COLLATE utf8mb3_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_players`
--

CREATE TABLE `stg_players` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(100) COLLATE utf8mb3_unicode_ci NOT NULL,
  `aliases` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)',
  `name_filter` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_scores`
--

CREATE TABLE `stg_scores` (
  `id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `player_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `player_name` varchar(100) COLLATE utf8mb3_unicode_ci NOT NULL,
  `score_value` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL,
  `played_at` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `score_value_real` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL,
  `score_value_sort` varchar(50) COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_score_attrs`
--

CREATE TABLE `stg_score_attrs` (
  `id` int(11) NOT NULL,
  `score_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(32) COLLATE utf8mb3_unicode_ci NOT NULL,
  `value` varchar(100) COLLATE utf8mb3_unicode_ci NOT NULL,
  `title` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_score_category_values`
--

CREATE TABLE `stg_score_category_values` (
  `id` int(11) NOT NULL,
  `score_id` int(11) NOT NULL,
  `category_value_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_score_sources`
--

CREATE TABLE `stg_score_sources` (
  `id` int(11) NOT NULL,
  `score_id` int(11) NOT NULL,
  `source_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `date` varchar(100) COLLATE utf8mb3_unicode_ci NOT NULL,
  `url` varchar(500) COLLATE utf8mb3_unicode_ci NOT NULL,
  `comment` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stg_sources`
--

CREATE TABLE `stg_sources` (
  `id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` longtext COLLATE utf8mb3_unicode_ci NOT NULL COMMENT '(DC2Type:json)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- --------------------------------------------------------

--
-- Structure for view `stg_query_companies`
--
DROP VIEW IF EXISTS `stg_query_companies`;

CREATE VIEW `stg_query_companies`  AS SELECT concat(`c`.`id`,'-',`l`.`value`) AS `id`, `c`.`created_at` AS `created_at`, `c`.`updated_at` AS `updated_at`, `c`.`id` AS `company_id`, `l`.`value` AS `locale`, json_unquote(coalesce(json_extract(`c`.`name`,concat('$.',`l`.`value`)),json_extract(`c`.`name`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `name`, json_unquote(coalesce(json_extract(`c`.`name_translit`,concat('$.',`l`.`value`)),json_extract(`c`.`name_translit`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `name_translit`, `c`.`name_filter` AS `name_filter` FROM (`stg_companies` `c` join `stg_locales` `l`)  ;

-- --------------------------------------------------------

--
-- Structure for view `stg_query_games`
--
DROP VIEW IF EXISTS `stg_query_games`;

CREATE VIEW `stg_query_games`  AS SELECT concat(`g`.`id`,'-',`l`.`value`) AS `id`, `g`.`created_at` AS `created_at`, `g`.`updated_at` AS `updated_at`, `g`.`id` AS `game_id`, `l`.`value` AS `locale`, json_unquote(coalesce(json_extract(`g`.`name`,concat('$.',`l`.`value`)),json_extract(`g`.`name`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `name`, json_unquote(coalesce(json_extract(`g`.`name_translit`,concat('$.',`l`.`value`)),json_extract(`g`.`name_translit`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `name_translit`, `g`.`name_filter` AS `name_filter`, `g`.`company_id` AS `company_id`, `c`.`name` AS `company_name`, `c`.`name_translit` AS `company_name_translit`, `c`.`name_filter` AS `company_name_filter`, json_unquote(coalesce(json_extract(`g`.`description`,concat('$.',`l`.`value`)),json_extract(`g`.`description`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `description`, json_unquote(coalesce(json_extract(`g`.`translations`,concat('$.',`l`.`value`)),json_extract(`g`.`translations`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `translations` FROM ((`stg_games` `g` join `stg_locales` `l`) join `stg_query_companies` `c` on(`c`.`company_id` = `g`.`company_id` and `c`.`locale` = `l`.`value`))  ;

-- --------------------------------------------------------

--
-- Structure for view `stg_query_players`
--
DROP VIEW IF EXISTS `stg_query_players`;

CREATE VIEW `stg_query_players`  AS SELECT concat(`p`.`id`,'-',`l`.`value`) AS `id`, `p`.`created_at` AS `created_at`, `p`.`updated_at` AS `updated_at`, `p`.`id` AS `player_id`, `l`.`value` AS `locale`, `p`.`name` AS `name`, `p`.`aliases` AS `aliases`, `p`.`name_filter` AS `name_filter` FROM (`stg_players` `p` join `stg_locales` `l`)  ;

-- --------------------------------------------------------

--
-- Structure for view `stg_query_scores`
--
DROP VIEW IF EXISTS `stg_query_scores`;

CREATE VIEW `stg_query_scores`  AS SELECT concat(`s`.`id`,'-',`l`.`value`) AS `id`, `s`.`created_at` AS `created_at`, `s`.`updated_at` AS `updated_at`, `s`.`id` AS `score_id`, `l`.`value` AS `locale`, `s`.`game_id` AS `game_id`, `g`.`name` AS `game_name`, `g`.`name_translit` AS `game_name_translit`, `g`.`name_filter` AS `game_name_filter`, `g`.`company_id` AS `company_id`, `g`.`company_name` AS `company_name`, `g`.`company_name_translit` AS `company_name_translit`, `g`.`company_name_filter` AS `company_name_filter`, `s`.`player_id` AS `player_id`, `s`.`player_name` AS `player_name`, `s`.`played_at` AS `played_at`, `s`.`score_value` AS `score_value`, `s`.`score_value_real` AS `score_value_real`, `s`.`score_value_sort` AS `score_value_sort` FROM ((`stg_scores` `s` join `stg_locales` `l`) join `stg_query_games` `g` on(`g`.`game_id` = `s`.`game_id` and `g`.`locale` = `l`.`value`))  ;

-- --------------------------------------------------------

--
-- Structure for view `stg_query_score_attrs`
--
DROP VIEW IF EXISTS `stg_query_score_attrs`;

CREATE VIEW `stg_query_score_attrs`  AS SELECT concat(`a`.`id`,'-',`l`.`value`) AS `id`, `a`.`created_at` AS `created_at`, `a`.`updated_at` AS `updated_at`, `a`.`id` AS `attr_id`, `l`.`value` AS `locale`, `a`.`score_id` AS `score_id`, `a`.`name` AS `name`, `a`.`value` AS `value`, json_unquote(coalesce(json_extract(`a`.`title`,concat('$.',`l`.`value`)),json_extract(`a`.`title`,concat('$.',(select `stg_locales`.`value` from `stg_locales` where `stg_locales`.`is_default` = 1))))) AS `title` FROM (`stg_score_attrs` `a` join `stg_locales` `l`)  ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stg_companies`
--
ALTER TABLE `stg_companies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stg_counterstops`
--
ALTER TABLE `stg_counterstops`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_117D46BE48FD905` (`game_id`);

--
-- Indexes for table `stg_games`
--
ALTER TABLE `stg_games`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8DF560B1979B1AD6` (`company_id`);

--
-- Indexes for table `stg_game_categories`
--
ALTER TABLE `stg_game_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_D568ABC7E48FD905` (`game_id`);

--
-- Indexes for table `stg_game_category_values`
--
ALTER TABLE `stg_game_category_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_CB8DAF0D12469DE2` (`category_id`);

--
-- Indexes for table `stg_game_links`
--
ALTER TABLE `stg_game_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_8A4FE5CFE48FD905` (`game_id`),
  ADD KEY `IDX_8A4FE5CFE559DFD1` (`locale_id`);

--
-- Indexes for table `stg_locales`
--
ALTER TABLE `stg_locales`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_3F16263E1D775834` (`value`);

--
-- Indexes for table `stg_players`
--
ALTER TABLE `stg_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stg_scores`
--
ALTER TABLE `stg_scores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_EA9A6235E48FD905` (`game_id`),
  ADD KEY `IDX_EA9A623599E6F5DF` (`player_id`);

--
-- Indexes for table `stg_score_attrs`
--
ALTER TABLE `stg_score_attrs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_DD458E2612EB0A51` (`score_id`);

--
-- Indexes for table `stg_score_category_values`
--
ALTER TABLE `stg_score_category_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_9B51749412EB0A51` (`score_id`),
  ADD KEY `IDX_9B5174944E5A3FD9` (`category_value_id`);

--
-- Indexes for table `stg_score_sources`
--
ALTER TABLE `stg_score_sources`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_E197A8A212EB0A51` (`score_id`),
  ADD KEY `IDX_E197A8A2953C1C61` (`source_id`);

--
-- Indexes for table `stg_sources`
--
ALTER TABLE `stg_sources`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stg_companies`
--
ALTER TABLE `stg_companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_counterstops`
--
ALTER TABLE `stg_counterstops`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_games`
--
ALTER TABLE `stg_games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_game_categories`
--
ALTER TABLE `stg_game_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_game_category_values`
--
ALTER TABLE `stg_game_category_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_game_links`
--
ALTER TABLE `stg_game_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_locales`
--
ALTER TABLE `stg_locales`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_players`
--
ALTER TABLE `stg_players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_scores`
--
ALTER TABLE `stg_scores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_score_attrs`
--
ALTER TABLE `stg_score_attrs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_score_category_values`
--
ALTER TABLE `stg_score_category_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_score_sources`
--
ALTER TABLE `stg_score_sources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stg_sources`
--
ALTER TABLE `stg_sources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `stg_counterstops`
--
ALTER TABLE `stg_counterstops`
  ADD CONSTRAINT `FK_117D46BE48FD905` FOREIGN KEY (`game_id`) REFERENCES `stg_games` (`id`);

--
-- Constraints for table `stg_games`
--
ALTER TABLE `stg_games`
  ADD CONSTRAINT `FK_8DF560B1979B1AD6` FOREIGN KEY (`company_id`) REFERENCES `stg_companies` (`id`);

--
-- Constraints for table `stg_game_categories`
--
ALTER TABLE `stg_game_categories`
  ADD CONSTRAINT `FK_D568ABC7E48FD905` FOREIGN KEY (`game_id`) REFERENCES `stg_games` (`id`);

--
-- Constraints for table `stg_game_category_values`
--
ALTER TABLE `stg_game_category_values`
  ADD CONSTRAINT `FK_CB8DAF0D12469DE2` FOREIGN KEY (`category_id`) REFERENCES `stg_game_categories` (`id`);

--
-- Constraints for table `stg_game_links`
--
ALTER TABLE `stg_game_links`
  ADD CONSTRAINT `FK_8A4FE5CFE48FD905` FOREIGN KEY (`game_id`) REFERENCES `stg_games` (`id`),
  ADD CONSTRAINT `FK_8A4FE5CFE559DFD1` FOREIGN KEY (`locale_id`) REFERENCES `stg_locales` (`id`);

--
-- Constraints for table `stg_scores`
--
ALTER TABLE `stg_scores`
  ADD CONSTRAINT `FK_EA9A623599E6F5DF` FOREIGN KEY (`player_id`) REFERENCES `stg_players` (`id`),
  ADD CONSTRAINT `FK_EA9A6235E48FD905` FOREIGN KEY (`game_id`) REFERENCES `stg_games` (`id`);

--
-- Constraints for table `stg_score_attrs`
--
ALTER TABLE `stg_score_attrs`
  ADD CONSTRAINT `FK_DD458E2612EB0A51` FOREIGN KEY (`score_id`) REFERENCES `stg_scores` (`id`);

--
-- Constraints for table `stg_score_category_values`
--
ALTER TABLE `stg_score_category_values`
  ADD CONSTRAINT `FK_9B51749412EB0A51` FOREIGN KEY (`score_id`) REFERENCES `stg_scores` (`id`),
  ADD CONSTRAINT `FK_9B5174944E5A3FD9` FOREIGN KEY (`category_value_id`) REFERENCES `stg_game_category_values` (`id`);

--
-- Constraints for table `stg_score_sources`
--
ALTER TABLE `stg_score_sources`
  ADD CONSTRAINT `FK_E197A8A212EB0A51` FOREIGN KEY (`score_id`) REFERENCES `stg_scores` (`id`),
  ADD CONSTRAINT `FK_E197A8A2953C1C61` FOREIGN KEY (`source_id`) REFERENCES `stg_sources` (`id`);
COMMIT;
