<?php

use App\Models\User;

it('shows companies', function () {
    actingAs(User::factory()->create())
        ->get('/admin/companies')
        ->assertSee('Companies');
});

it('edits a company', function () {
    actingAs(User::factory()->create())
        ->get('/admin/companies/1/edit')
        ->assertSee('Edit ADK');
});
