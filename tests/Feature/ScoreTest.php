<?php

use App\Models\User;

it('shows scores', function () {
    actingAs(User::factory()->create())
        ->get('/admin/scores')
        ->assertSee('Scores');
});

it('edits a score', function () {
    actingAs(User::factory()->create())
        ->get('/admin/scores/8/edit')
        ->assertSee('Edit Score')
        ->assertSee('14,734,400');
});
