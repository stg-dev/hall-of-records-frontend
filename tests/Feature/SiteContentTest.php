<?php

use App\Models\User;

it('shows sitecontents', function () {
    actingAs(User::factory()->create())
        ->get('/admin/site-contents')
        ->assertSee('Site Contents');
});

it('edits a player', function () {
    actingAs(User::factory()->create())
        ->get('/admin/site-contents/1/edit')
        ->assertSee('Edit welcome');
});
