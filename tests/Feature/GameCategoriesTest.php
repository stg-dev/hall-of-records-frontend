<?php

use App\Models\User;

it('shows companies', function () {
    actingAs(User::factory()->create())
        ->get('/admin/game-categories')
        ->assertSee('Game Categories');
});

it('edits a company', function () {
    actingAs(User::factory()->create())
        ->get('/admin/game-categories/11/edit')
        ->assertSee('Edit Game Category');
});
