<?php

use App\Models\User;

it('shows games', function () {
    actingAs(User::factory()->create())
        ->get('/admin/games')
        ->assertSee('Games');
});

it('edits a game', function () {
    actingAs(User::factory()->create())
        ->get('/admin/games/1/edit')
        ->assertSee('Edit 1941 Counter Attack');
});
