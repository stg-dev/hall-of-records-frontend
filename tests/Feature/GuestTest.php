<?php

foreach (
    [
    'companies',
    'game-categories',
    'games',
    'players',
    'scores',
    'site-contents',
    'sources',
    ] as $url
) {
    it("shows a login screen when visiting admin/$url as guest", function () use ($url) {
        $this
            ->followingRedirects()
            ->get("/admin/$url")
            ->assertStatus(200)
            ->assertSee('Sign in to your account');
    });
}
