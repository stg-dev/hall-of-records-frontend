<?php

use App\Models\User;

it('shows players', function () {
    actingAs(User::factory()->create())
        ->get('/admin/players')
        ->assertSee('Players');
});

it('edits a player', function () {
    actingAs(User::factory()->create())
        ->get('/admin/players/8/edit')
        ->assertSee('Edit PIA-CNT!');
});
