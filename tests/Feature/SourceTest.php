<?php

use App\Models\User;

it('shows sources', function () {
    actingAs(User::factory()->create())
        ->get('/admin/sources')
        ->assertSee('Sources');
});

it('edits a source', function () {
    actingAs(User::factory()->create())
        ->get('/admin/sources/1/edit')
        ->assertSee('Edit Gamest');
});
