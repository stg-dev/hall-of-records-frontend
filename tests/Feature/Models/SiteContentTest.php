<?php

namespace Tests\Feature\Models;

use Tests\TestCase;

/**
 * @internal
 * @coversNothing
 */
class SiteContentTest extends TestCase
{
    public function testWelcome()
    {
        $response = $this->get('/');

        $response->assertSee('Welcome to the STG Hall of Records!');
        $response->assertStatus(200);
    }
}
