<header class="float-left">
    <navbar>
    <div class="absolute top-0 p-6 bg-white w-60">
        <div class="flex mb-6 space-x-6">
            <h1>Hall of Records</h1>
        </div>
        <ul class="flex flex-col py-6 border-t space-y-6 mt-14">
            <li class=""><a class="link {{ Route::current()->uri == '/' ? 'font-bold' : '' }}" href="/">Main Page</a></li>
            <li> Navigation </li>
            <li class=""><a class="link {{ Route::current()->uri == 'companies' ? 'font-bold' : '' }}" href="/companies">Companies</a></li>
            <li class=""><a class="link {{ Route::current()->uri == 'games' ? 'font-bold' : '' }}" href="/games">Games</a></li>
            <li class=""><a class="link {{ Route::current()->uri == 'players' ? 'font-bold' : '' }}" href="/players">Players</a></li>
            <li> Languages </li>
            <li class=""><a class="link" href="">English</a></li>
            <li class=""><a class="link" href="">日本語</a></li>
        </ul>
    </div>
    </navbar>
</header>
