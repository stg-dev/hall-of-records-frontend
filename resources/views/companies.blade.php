<x-guest-layout>
    <div class="flex flex-row overflow-hidden">
        <div class="flex-1 px-5 m-10 my-5 text-gray-500 border-2">
            <p> Search for Company </p>

            <livewire:game-record type="companies" />
        </div>
    </div>
</x-guest-layout>
