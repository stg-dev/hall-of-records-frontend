<div>
    <div class="flex items-center justify-center gap-6">
        <div class="form-control">
            <label class="label">
                <span class="label-text">Player</span>
            </label>
            <input required type="text" placeholder="name of the player" class="input input-bordered" wire:model="player_name">
            @error('player') <span class="error">{{ $message }}</span> @enderror
        </div>

        <div class="form-control">
            <label class="label">
                <span class="label-text">Achieved Score</span>
            </label>
            <input required type="text" placeholder="achieved score in numbers only" class="input input-bordered" wire:model="score">
            @error('score') <span class="error">{{ $message }}</span> @enderror
        </div>

        <a class="btn btn-primary" wire:click="addScore()"> Add Score </a>
    </div>
</div>
