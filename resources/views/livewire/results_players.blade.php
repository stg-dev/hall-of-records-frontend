<div>
    @include('livewire.search')

    @if ($results->isEmpty())
        <div class="flex items-center justify-center ">
            <p> No players found with the search string <strong> {{ $search }} </strong>. </p>
        </div>
    @else
        <table class="table float-left table-zebra table-compact hover">
            <thead>
                <tr>
                    <th>Player </th>
                </tr>
            </thead>
            <tbody>
                @foreach ($results as $player)
                    <tr>
                        <td> {{ $player->player_name }} </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @endif
</div>
