<div>
    @include('livewire.search')

    @if ($results->isEmpty())
        <div class="flex items-center justify-center ">
            <p> No games found with the search string <strong> {{ $search }} </strong>. </p>
        </div>
    @else
        <table class="table float-left table-zebra table-compact hover">
            <tr>
                <th> Game </th>
            </tr>
            @foreach ($results as $game)
                <tr>
                    <td> <a class="link" wire:click="">
                            {{ $game->name_filter }} <br />
                            ({{ $game->company->name_filter }})
                        </a> </td>
                </tr>
            @endforeach
        </table>
    @endif
</div>
