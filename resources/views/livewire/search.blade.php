<div class="flex flex-row items-center justify-center w-full m-6 gap-6">
    <input
        style="width: 400px;"
        type="string"
        wire:model.debounce.500ms="search"
        class="form-control input"
        placeholder="Search" />

    <p> <strong> {{ count($results) }} </strong> results found: </p>

    <a class="link btn btn-primary" wire:click="searchBy('')"> Clear Search </a>
</div>

<div wire:loading>
    <div class="flex items-center justify-center ">
        <div class="w-16 h-16 border-b-2 border-gray-900 rounded-full animate-spin"></div>
    </div>
</div>
