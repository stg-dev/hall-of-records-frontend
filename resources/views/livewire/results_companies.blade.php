<div>
    @include('livewire.search')

    @if ($results->isEmpty())
        <div class="flex items-center justify-center ">
            <p> No companies found with the search string <strong> {{ $search }} </strong>. </p>
        </div>
    @else
        <table class="table float-left table-zebra table-compact hover">
            <tr>
                <th> Company </th>
            </tr>
            @foreach ($results as $company)
                <tr>
                    <td> <a class="link" wire:click="searchBy('{{ $company->name_filter }}')"> {{ $company->name_filter }} </a> </td>
                </tr>
            @endforeach
        </table>
    @endif
</div>
