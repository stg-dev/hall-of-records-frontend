<x-guest-layout>
    <div class="flex flex-row overflow-hidden">
        <div class="flex-1 px-5 m-10 my-5 text-gray-500 border-2">
            <livewire:score-table />
        </div>
    </div>
    <div class="flex flex-row overflow-hidden">
        <div class="flex-1 px-5 m-10 my-5 text-gray-500 border-2">
            <livewire:game-table />
        </div>

        <div class="flex-1 px-5 m-10 my-5 text-gray-500 border-2">
            <livewire:player-table />
        </div>
    </div>
</x-guest-layout>
