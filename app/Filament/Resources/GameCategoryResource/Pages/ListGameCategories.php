<?php

namespace App\Filament\Resources\GameCategoryResource\Pages;

use App\Filament\Resources\GameCategoryResource;
use Filament\Resources\Pages\ListRecords;

class ListGameCategories extends ListRecords
{
    protected static string $resource = GameCategoryResource::class;
}
