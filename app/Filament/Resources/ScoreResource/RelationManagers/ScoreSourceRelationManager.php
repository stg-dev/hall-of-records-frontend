<?php

namespace App\Filament\Resources\ScoreResource\RelationManagers;

use App\Models\ScoreSource;
use App\Models\Source;
use Filament\Forms;
use Filament\Forms\Components\DatePicker;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyThroughRelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;

class ScoreSourceRelationManager extends HasManyThroughRelationManager
{
    protected static string $relationship = 'sources';

    protected static ?string $recordTitleAttribute = 'url';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextColumn::make('date'),
                Select::make('source_id')
                    ->required()
                    ->label('Source')
                    ->searchable()
                    ->options(Source::all()->pluck('name', 'id')),
                TextInput::make('url')->url()->columnSpan(2),
                MarkdownEditor::make('comment'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('date')->sortable(),
                TextColumn::make('source.name')->label('Source')->sortable(),
                TextColumn::make('score.player_name')->label('Player')->sortable(),
                TextColumn::make('url')
                    ->url(fn (ScoreSource $record): string => $record->url)
                    ->openUrlInNewTab()
                    ->sortable(),
            ])
            ->filters([
                //
            ]);
    }
}
