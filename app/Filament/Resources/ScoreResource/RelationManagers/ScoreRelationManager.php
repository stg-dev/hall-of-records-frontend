<?php

namespace App\Filament\Resources\ScoreResource\RelationManagers;

use App\Models\Player;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class ScoreRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'scores';

    protected static ?string $recordTitleAttribute = 'score_value';

    public static function form(Form $form): Form
    {
        return $form->schema([
            TextInput::make('played_at'),
            TextInput::make('player_name'),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('played_at'),
            TextColumn::make('player_name'),
            TextColumn::make('game.name'),
            TextColumn::make('score_value'),
        ])->filters([]);
    }
}
