<?php

namespace App\Filament\Resources;

use App\Filament\Resources\GameResource\Pages;
use App\Filament\Resources\GameResource\RelationManagers\CategoriesRelationManager;
use App\Filament\Resources\GameResource\RelationManagers\CounterstopsRelationManager;
use App\Filament\Resources\GameResource\RelationManagers\LinksRelationManager;
use App\Filament\Resources\GameResource\RelationManagers\ScoreSourceRelationManager;
use App\Filament\Resources\ScoreResource\RelationManagers\ScoreRelationManager;
use App\Models\Company;
use App\Models\Game;
use Filament\Forms\Components\MarkdownEditor;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Concerns\Translatable;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class GameResource extends Resource
{
    use Translatable;

    protected static ?string $model = Game::class;

    protected static ?string $navigationIcon = 'heroicon-o-play';

    protected static ?string $recordTitleAttribute = 'name';

    public static function form(Form $form): Form
    {
        return $form->schema([
            TextInput::make('name')->required(),
            TextInput::make('name_translit')
                ->label('Transliteration')
                ->required(),
            TextInput::make('name_filter')
                ->label('Search Term the game gets found')
                ->required(),
            Select::make('company_id')
                ->required()
                ->label('Company')
                ->searchable()
                ->options(Company::all()->pluck('name', 'id')),
            MarkdownEditor::make('description')->columnSpan(2),
        ])->columns(2);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('name'),
            TextColumn::make('company.name'),
        ])->filters([]);
    }

    public static function getRelations(): array
    {
        return [
            ScoreRelationManager::class,
            CounterstopsRelationManager::class,
            CategoriesRelationManager::class,
            LinksRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGames::route('/'),
            'create' => Pages\CreateGame::route('/create'),
            'edit' => Pages\EditGame::route('/{record}/edit'),
        ];
    }
}
