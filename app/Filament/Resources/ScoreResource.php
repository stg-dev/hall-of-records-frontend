<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ScoreResource\Pages;
use App\Filament\Resources\ScoreResource\RelationManagers\ScoreSourceRelationManager;
use App\Models\Score;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class ScoreResource extends Resource
{
    protected static ?string $model = Score::class;

    protected static ?string $navigationIcon = 'heroicon-o-calendar';

    protected static ?string $recordTitleAttribute = 'score_value';

    public static function form(Form $form): Form
    {
        return $form->schema([
            TextInput::make('score_value')->required(),
            TextInput::make('score_value_real')->required(),
            TextInput::make('score_value_sort')->required(),
            TextInput::make('player_name')->required(),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('game.name'),
            TextColumn::make('player_name'),
            TextColumn::make('score_value'),
        ])->filters([]);
    }

    public static function getRelations(): array
    {
        return [
            ScoreSourceRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListScores::route('/'),
            'create' => Pages\CreateScore::route('/create'),
            'edit' => Pages\EditScore::route('/{record}/edit'),
        ];
    }
}
