<?php

namespace App\Filament\Resources\CompanyResource\RelationManagers;

use App\Models\Game;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class GamesRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'games';

    protected static ?string $recordTitleAttribute = 'name';

    public static function form(Form $form): Form
    {
        return $form->schema([
            TextInput::make('name')->required(),
            TextInput::make('name_translit')
                ->label('Transliteration')
                ->required(),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('name')
                ->url(fn (Game $record): string => route('filament.resources.games.edit', ['record' => $record])),
            TextColumn::make('name_translit')
                ->url(fn (Game $record): string => route('filament.resources.games.edit', ['record' => $record])),
        ]);
    }
}
