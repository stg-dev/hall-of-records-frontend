<?php

namespace App\Filament\Resources\PlayerResource\RelationManagers;

use App\Models\Game;
use Filament\Forms;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;

class ScoreRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'scores';

    protected static ?string $recordTitleAttribute = 'score_value';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
            TextInput::make('played_at'),
            TextInput::make('player_name'),
            Select::make('game_id')
                ->required()
                ->label('Game')
                ->searchable()
                ->options(Game::all()->pluck('name', 'id')),
            TextInput::make('score_value'),
            TextInput::make('score_real'),
            TextInput::make('score_sort'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
            TextColumn::make('played_at'),
            TextColumn::make('player_name'),
            TextColumn::make('game.name'),
            TextColumn::make('score_value'),
            ])
            ->filters([
                //
            ]);
    }
}
