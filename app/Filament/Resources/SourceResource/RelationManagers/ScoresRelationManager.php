<?php

namespace App\Filament\Resources\SourceResource\RelationManagers;

use App\Models\Score;
use App\Models\ScoreSource;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;

class ScoresRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'Scores';

    protected static ?string $recordTitleAttribute = 'url';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('url')->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('date'),
                TextColumn::make('score.game.name')->label('Game'),
                TextColumn::make('score.player_name')->label('Player'),
                TextColumn::make('url')
                    ->url(fn (ScoreSource $record): string => $record->url)
                    ->openUrlInNewTab(),
                TextColumn::make('comment'),
            ])
            ->filters([
                //
            ]);
    }
}
