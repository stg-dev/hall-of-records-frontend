<?php

namespace App\Filament\Resources\GameResource\RelationManagers;

use App\Models\Locale;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class LinksRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'links';

    protected static ?string $recordTitleAttribute = 'url';

    public static function form(Form $form): Form
    {
        return $form->schema([
            Select::make('locale_id')
                ->required()
                ->label('Locale')
                ->options(Locale::all()->pluck('title', 'id')),
            TextInput::make('title')->required(),
            TextInput::make('url')->required(),
        ]);
    }

    public static function table(Table $table): Table
    {
        return $table->columns([
            TextColumn::make('locale.title'),
            TextColumn::make('title'),
            TextColumn::make('url'),
        ])->filters([]);
    }
}
