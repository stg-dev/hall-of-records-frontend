<?php

namespace App\Filament\Resources\GameResource\RelationManagers;

use App\Models\GameCategory;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class CategoriesRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'categories';

    protected static ?string $recordTitleAttribute = 'name';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Select::make('name')->required()->options([
                    'version' => 'version',
                    'autofire' => 'autofire',
                    'platform' => 'platform',
                    'game' => 'game',
                    'mode' => 'mode',
                    'difficulty' => 'difficulty',
                    'ship' => 'ship',
                    'weapon' => 'weapon',
                    'loop' => 'loop',
                ]),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                    ->url(fn (GameCategory $record): string => route(
                        'filament.resources.game-categories.edit',
                        ['record' => $record]
                    )),
                TextColumn::make('valuesFormatted')->label('values')
                    ->url(fn (GameCategory $record): string => route(
                        'filament.resources.game-categories.edit',
                        ['record' => $record]
                    )),
            ])
            ->filters([
            ]);
    }
}
