<?php

namespace App\Filament\Resources\GameResource\RelationManagers;

use Filament\Forms\Components\Select;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\RelationManagers\HasManyRelationManager;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class CounterstopsRelationManager extends HasManyRelationManager
{
    protected static string $relationship = 'counterstops';

    protected static ?string $recordTitleAttribute = 'score';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Select::make('type')->required()->options(['soft', 'hard']),
                TextInput::make('score')->required(),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('type'),
                TextColumn::make('score'),
            ])
            ->filters([
            ]);
    }
}
