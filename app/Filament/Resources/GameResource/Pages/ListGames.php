<?php

namespace App\Filament\Resources\GameResource\Pages;

use App\Filament\Resources\GameResource;
use Filament\Resources\Pages\ListRecords;
use Filament\Resources\Pages\ViewRecord\Concerns\Translatable;

class ListGames extends ListRecords
{
    use Translatable;

    protected static string $resource = GameResource::class;
}
