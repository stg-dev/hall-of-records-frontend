<?php

namespace App\Filament\Resources;

use App\Filament\Resources\GameCategoryResource\Pages;
use App\Filament\Resources\GameCategoryResource\RelationManagers\ValuesRelationManager;
use App\Models\GameCategory;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables\Columns\TextColumn;

class GameCategoryResource extends Resource
{
    protected static ?string $model = GameCategory::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('name'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('game.name')->searchable(),
                TextColumn::make('name')->searchable(),
                TextColumn::make('valuesFormatted')->searchable(),
            ])
            ->filters([
            ]);
    }

    public static function getRelations(): array
    {
        return [
            ValuesRelationManager::class,
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListGameCategories::route('/'),
            'create' => Pages\CreateGameCategory::route('/create'),
            'edit' => Pages\EditGameCategory::route('/{record}/edit'),
        ];
    }
}
