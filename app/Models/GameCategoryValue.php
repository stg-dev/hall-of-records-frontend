<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class GameCategoryValue extends Model
{
    use HasFactory;

    public $fillable = ['value', 'category_id', 'translations'];

    public function category(): BelongsTo
    {
        return $this->belongsTo(GameCategory::class);
    }
}
