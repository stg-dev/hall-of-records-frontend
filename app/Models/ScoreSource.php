<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ScoreSource extends Model
{
    use HasFactory;

    public $fillable = [
        'source_id',
        'date',
        'url',
        'comment',
    ];

    public function score(): BelongsTo
    {
        return $this->belongsTo(Score::class);
    }

    public function source(): BelongsTo
    {
        return $this->belongsTo(Source::class);
    }
}
