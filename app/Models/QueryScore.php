<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $locale
 */
final class QueryScore extends Model
{
    use HasFactory;

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    protected $casts = [
        'company_name_filter' => 'array',
        'game_name_filter' => 'array',
        'sources' => 'array',
    ];

    public function game(): BelongsTo
    {
        return $this->belongsTo(QueryGame::class)
            ->where('locale', '=', $this->locale);
    }

    public function player(): BelongsTo
    {
        return $this->belongsTo(QueryPlayer::class)
            ->where('locale', '=', $this->locale);
    }

    public function attributes(): HasMany
    {
        return $this->hasMany(QueryScoreAttr::class, 'score_id')
            ->where('locale', '=', $this->locale);
    }
}
