<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ScoreCategoryValue extends Model
{
    use HasFactory;

    public $timestamps = false;

    public $fillable = ['category_value_id'];

    public function score(): BelongsTo
    {
        return $this->belongsTo(Score::class);
    }

    public function categoryValue(): BelongsTo
    {
        return $this->belongsTo(GameCategoryValue::class);
    }
}
