<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Player extends Model
{
    use HasFactory;

    public $fillable = [
        'aliases',
        'name',
    ];

    public function games(): HasManyThrough
    {
        return $this->hasManyThrough(
            Game::class,
            Score::class,
            'player_id',
            'game_id',
            'player_id',
            'game_id'
        )
            ->distinct();
    }

    public function scores(): HasMany
    {
        return $this->hasMany(Score::class, 'player_id');
    }
}
