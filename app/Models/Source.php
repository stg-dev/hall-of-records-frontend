<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class Source extends Model
{
    use HasFactory;
    use HasTranslations;

    public $translatable = [
        'name',
    ];

    public $fillable = [
        'name',
    ];

    public function scores(): HasMany
    {
        return $this->hasMany(ScoreSource::class);
    }
}
