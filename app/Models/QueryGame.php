<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * @property string $locale
 */
final class QueryGame extends Model
{
    use HasFactory;

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    protected $casts = [
        'company_name_filter' => 'array',
        'name_filter' => 'array',
        'translations' => 'array',
    ];

    public function company(): BelongsTo
    {
        return $this->belongsTo(QueryCompany::class)
            ->where('locale', '=', $this->locale);
    }

    public function players(): HasManyThrough
    {
        return $this->hasManyThrough(
            QueryPlayer::class,
            QueryScore::class,
            'game_id',
            'player_id',
            'game_id',
            'player_id'
        )
            ->distinct()
            ->where('query_players.locale', '=', $this->locale);
    }

    public function scores(): HasMany
    {
        return $this->hasMany(QueryScore::class, 'game_id')
            ->where('locale', '=', $this->locale);
    }
}
