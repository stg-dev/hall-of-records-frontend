<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Score extends Model
{
    use HasFactory;

    public $fillable = [
        'game_id',
        'player_id',
        'player_name',
        'played_at',
        'score_value',
        'score_value_real',
        'score_value_sort',
    ];

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }

    public function player(): BelongsTo
    {
        return $this->belongsTo(Player::class);
    }

    public function sources(): HasMany
    {
        return $this->hasMany(ScoreSource::class);
    }

    public function categoryValues(): HasMany
    {
        return $this->hasMany(ScoreCategoryValue::class);
    }
}
