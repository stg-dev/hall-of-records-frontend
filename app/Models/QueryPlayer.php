<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

/**
 * @property string $locale
 */
final class QueryPlayer extends Model
{
    use HasFactory;

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    protected $casts = [
        'aliases' => 'array',
        'name_filter' => 'array',
    ];

    public function games(): HasManyThrough
    {
        return $this->hasManyThrough(
            QueryGame::class,
            QueryScore::class,
            'player_id',
            'game_id',
            'player_id',
            'game_id'
        )
            ->distinct()
            ->where('query_games.locale', '=', $this->locale);
    }

    public function scores(): HasMany
    {
        return $this->hasMany(QueryScore::class, 'player_id')
            ->where('locale', '=', $this->locale);
    }
}
