<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Spatie\Translatable\HasTranslations;

class Company extends Model
{
    use HasFactory;
    use HasTranslations;

    public $translatable = [
        'name',
        'name_translit',
    ];

    public $fillable = [
        'name',
        'name_translit',
    ];

    public function games(): HasMany
    {
        return $this->hasMany(Game::class);
    }
}
