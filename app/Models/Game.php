<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Spatie\Translatable\HasTranslations;

class Game extends Model
{
    use HasFactory;
    use HasTranslations;

    public $translatable = [
        'name',
        'name_translit',
        'description',
        'links',
    ];

    public $fillable = [
        'company_id',
        'counterstops',
        'description',
        'links',
        'name',
        'name_translit',
        'name_filter',
    ];

    public function scores(): HasMany
    {
        return $this->hasMany(Score::class);
    }

    public function sources(): HasManyThrough
    {
        return $this->hasManyThrough(ScoreSource::class, Score::class);
    }

    public function counterstops(): HasMany
    {
        return $this->hasMany(Counterstop::class);
    }

    public function links(): HasMany
    {
        return $this->hasMany(GameLink::class);
    }

    public function players(): HasMany
    {
        return $this->hasMany(Player::class);
    }

    public function company(): BelongsTo
    {
        return $this->belongsTo(Company::class);
    }

    public function categories(): HasMany
    {
        return $this->hasMany(GameCategory::class);
    }
}
