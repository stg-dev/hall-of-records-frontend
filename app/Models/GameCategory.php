<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class GameCategory extends Model
{
    use HasFactory;

    public string $valuesFormatted;

    public $fillable = ['name', 'translations'];

    public static function defaults(): Builder
    {
        return self::query()->whereNull('game_id');
    }

    public function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }

    public function values(): HasMany
    {
        return $this->hasMany(GameCategoryValue::class, 'category_id');
    }

    protected function valuesFormatted(): Attribute
    {
        return new Attribute(
            get: function ($value) {
                $values = [];
                foreach ($this->values as $value) {
                    $values[] = $value->value;
                }

                return implode(', ', $values);
            },
        );
    }
}
