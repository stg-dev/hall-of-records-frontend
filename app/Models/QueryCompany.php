<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @property string $locale
 */
final class QueryCompany extends Model
{
    use HasFactory;

    public const CREATED_AT = null;
    public const UPDATED_AT = null;

    protected $casts = [
        'name_filter' => 'array',
    ];

    public function games(): HasMany
    {
        return $this->hasMany(QueryGame::class, 'company_id')
            ->where('locale', '=', $this->locale);
    }
}
