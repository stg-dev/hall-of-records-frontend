<?php

namespace App\Http\Controllers;

use App\Models\SiteContent;

class SiteContentController extends Controller
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function view(?SiteContent $sitecontent = null)
    {
        if (!$sitecontent) {
            $sitecontent = SiteContent::findOrFail(1); // welcome
        }

        return response()->view('sitecontent', ['sitecontent' => $sitecontent]);
    }
}
