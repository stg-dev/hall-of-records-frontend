<?php

namespace App\Http\Livewire;

use App\Models\QueryCompany;
use App\Models\QueryGame;
use App\Models\QueryScore;
use Illuminate\Support\Collection;
use Livewire\Component;

class GameRecord extends Component
{
    public string $search = '';

    public string $type = '';

    public Collection $results;

    protected $queryString = [
        'search' => ['except' => ''],
    ];

    // https://stackoverflow.com/questions/66094788/laravel-livewire-how-to-force-parent-component-refresh
    protected $listeners = ['reRenderParent'];

    public function reRenderParent()
    {
        // @phpstan-ignore-next-line
        $this->mount();
        $this->render();
    }

    public function render()
    {
        $this->results = new Collection();

        switch ($this->type) {
            case 'players':
                $this->results = QueryScore::query()
                    ->where('player_name', 'like', "%{$this->search}%")
                    ->orderBy('score_value_sort', 'ASC')
                    ->get();

                break;

            case 'companies':
                $this->results = QueryCompany::query()
                    ->where('name_filter', 'like', "%{$this->search}%")
                    ->get();

                break;

            case 'games':
                $this->results = QueryGame::query()
                    ->where('games.name_filter', 'like', "%{$this->search}%")
                    ->orderBy('games.name_filter', 'ASC')
                    ->get();

                break;
        }

        return view("livewire.results_{$this->type}");
    }

    public function searchBy(string $search)
    {
        $this->search = $search;
    }
}
