<?php

namespace App\Http\Livewire;

use App\Models\Score;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class ScoreTable extends LivewireDatatable
{
    public function builder()
    {
        return Score::query();
    }

    public function columns()
    {
        return [
            Column::name('created_at')->searchable(),
            Column::name('game_id')->searchable(),
            Column::name('player_name')->searchable(),
            Column::name('score_value')->searchable()->defaultSort('asc'),
        ];
    }
}
