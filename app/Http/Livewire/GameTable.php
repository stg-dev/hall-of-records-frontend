<?php

namespace App\Http\Livewire;

use App\Models\Game;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class GameTable extends LivewireDatatable
{
    public $perPage = 20;

    public function builder()
    {
        return Game::query();
    }

    public function columns()
    {
        return [
            Column::name('name_filter')
                ->label('Game')
                ->defaultSort('asc')
                ->searchable(),
        ];
    }
}
