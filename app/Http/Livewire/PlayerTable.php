<?php

namespace App\Http\Livewire;

use App\Models\Player;
use Mediconesystems\LivewireDatatables\Column;
use Mediconesystems\LivewireDatatables\Http\Livewire\LivewireDatatable;

class PlayerTable extends LivewireDatatable
{
    public $perPage = 20;

    public function builder()
    {
        return Player::query();
    }

    public function columns()
    {
        return [
            Column::name('name_filter')
                ->label('Player')
                ->defaultSort('asc')
                ->searchable(),
        ];
    }
}
