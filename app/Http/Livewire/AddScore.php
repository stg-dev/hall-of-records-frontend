<?php

namespace App\Http\Livewire;

use App\Models\Game;
use App\Models\Score;
use Livewire\Component;

class AddScore extends Component
{
    public int $gameId;

    public ?Game $game = null;

    // the new score to be added
    public ?string $score = null;

    // if existing, the id of the player that has achieved the score
    public int $player_id = 0;

    // if not yet existing, the string of the _new_ player that has achieved the score
    public string $player_name = '';

    protected $rules = [
        'score' => 'required|integer',
        'player_name' => 'required|string',
    ];

    public function render()
    {
        $this->game = Game::find($this->gameId);

        return view('livewire.add-score');
    }

    public function addScore()
    {
        $this->validate();

        $score = new Score();
        $score->score_value_sort = $this->score;
        $score->score_value_real = $this->score;
        $score->score_value = $this->score;
        $score->player_name = $this->player_name;
        $score->game_id = $this->gameId;
        $score->save();
        $this->emit('reRenderParent');
    }
}
