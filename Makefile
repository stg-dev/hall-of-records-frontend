MAKEFLAGS=-s

help:
	echo "Possible commands are:\n"
	cat ${MAKEFILE_LIST} | grep -v "{MAKEFILE_LIST} | grep -v" | grep ":.*##" | column -t -s "##"

install: ## install hall-of-records
	touch .env
	cp .env .env.orig
	cp .env.example .env
	docker run --rm -u "$$(id -u):$$(id -g)" -v $$(pwd):/var/www/html -w /var/www/html composer:2 composer install --ignore-platform-reqs
	./vendor/bin/sail down
	./vendor/bin/sail up -d
	./vendor/bin/sail artisan key:generate
	./vendor/bin/sail npm install
	${MAKE} demo-data-fresh
	./vendor/bin/sail restart workspace
	${MAKE} deployment.phar

demo-data-fresh:
	rm -f database/dumps/demo_data.sql
	./vendor/bin/sail artisan migrate:fresh
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\AdminSeeder
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\DemoDataSeeder
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\SiteContentSeeder
	${MAKE} create_test_database

deployment.phar:
	wget https://github.com/dg/ftp-deployment/releases/download/v3.5.2/deployment.phar

create_test_database: ## create the test databases on mariadb. Should only run once.
	docker exec hall-of-records_workspace_1 mysql -uroot -ppassword -hmariadb hall_of_records -e "create database hall_of_records_test" || true

inspect:
	./vendor/bin/phpcs
	./vendor/bin/phpstan analyse --memory-limit=1G

test: ## migrate fresh and run all tests
	./vendor/bin/sail artisan migrate:fresh --env=testing
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\DemoDataSeeder --env=testing
	./vendor/bin/sail artisan db:seed --class=Database\\Seeders\\SiteContentSeeder --env=testing
	./vendor/bin/sail artisan test

deploy: ## deploy to hallofrecords.net. Requires FTP_REMOTE environment variable.
	composer install --no-dev
	npm install
	npm run prod
	php ./deployment.phar ${PWD}/deployment.php
