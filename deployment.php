<?php

return [
    'remote' => getenv('FTP_REMOTE'),
    'ignore' => [
        '.gitignore',
        '.env',
        '.env.example',
        'node_modules/',
        'artisan',
        'deployment.ini',
        'docker-compose.yml',
        'package.json',
        'package-lock.json',
        'phpunit.xml',
        'tests/',
        'storage/',
    ],
];
